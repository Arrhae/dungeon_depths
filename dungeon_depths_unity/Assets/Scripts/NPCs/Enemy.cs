﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public abstract class Enemy : NPC
    {
        public Enemy(ICombatantMaster combatantMaster) : base(combatantMaster)
        {

        }
    }
}