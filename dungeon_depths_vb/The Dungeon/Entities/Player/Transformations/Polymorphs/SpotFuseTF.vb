﻿Public Class SpotFuseTF
    Inherits PolymorphTF
    Sub New()
        MyBase.New()
        tfName = "SpotFuseTF"
    End Sub
    Sub New(cs As Integer, n As Integer, tts As Integer, wi As Double, cbs As Boolean, tfd As Boolean)
        MyBase.New(cs, n, tts, wi, cbs, tfd)
        nextStep = getNextStep(cs)
    End Sub

    Public Overrides Sub setWaitTime(stage As Integer)
        turnsTilNextStep = 100
    End Sub

    Public Overrides Sub step1()
    End Sub
End Class
